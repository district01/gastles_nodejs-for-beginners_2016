var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var messageSchema = new Schema({
  name: String,
  content: String,
  room: String
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

// Since our clientside app requires Uppercase names,
// we create them as virtuals and keep them in sync with our lower case properties in the database

// Create a virtual `Name` and sync it with the `name` property
messageSchema.virtual('Name')
  .get(function () {
    // when someone requests `message.Name` give him the `message.name`
    return this.name;
  }).set(function (Name) {
    // when someone changes the `message.Name` save it to the `message.name`
    this.set('name', Name);
  });

// Create a virtual `Content` and sync it with the `content` property
messageSchema.virtual('Content')
  .get(function () {
    // when someone requests `message.Content` give him the `message.content`
    return this.content;
  }).set(function (Content) {
    // when someone changes the `message.Content` save it to the `message.content`
    this.set('content', Content);
  });

// Create a virtual `Room` and sync it with the `room` property
messageSchema.virtual('Room')
  .get(function () {
    // when someone requests `message.Room` give him the `message.room`
    return this.room;
  }).set(function (Room) {
    // when someone changes the `message.Room` save it to the `message.room`
    this.set('room', Room);
  });

// Create a virtual `messageId` and sync it with the `_id` property
messageSchema.virtual('MessageId')
  .get(function () {
    // when someone requests `message.messageId` give him the `message._id`
    return this._id;
  });

// Create a virtual `timestamp` and sync it with the `_id` property
messageSchema.virtual('Timestamp')
  .get(function () {
    // when someone requests `message.timestamp` give him the `message._id`s timestamp representation
    return this._id.getTimestamp();
  });

// Because we don't want all properties in the output, we clean up the model in a toJSON transformer
messageSchema.options.toJSON = {
    virtuals: true,
    transform: function(document, returnedObject, options) {
        // remove the `name` as it is exposed as the virtual `Name`
        delete returnedObject.name;
        // remove the `content` as it is exposed as the virtual `Content`
        delete returnedObject.content;
        // remove the `room` as it is exposed as the virtual `Room`
        delete returnedObject.room;
        // remove the `_id` as it is exposed as the virtual `MessageId`
        delete returnedObject._id;
        // remove the `id` as it is exposed as the virtual `MessageId`
        delete returnedObject.id;
        // remove the `__v` as it is an internal variable
        delete returnedObject.__v;

        return returnedObject;
    }
};

var Message = mongoose.model('Message', messageSchema);

module.exports = Message;
