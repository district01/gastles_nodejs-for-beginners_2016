# Seminar NodeJS 2016

## The slides

https://docs.google.com/presentation/d/1lZERH6miJ1OHo-3HnsgBpaQa09mk9TA13MoAliobLUE/edit

## Code usage

* clone this repository with `git clone https://bitbucket.org/district01/gastles-nodejs-for-beginners-2016.git`
* after you checkout a certain branch always `npm install`
* if you want to go back to the start of a branch (reset your changes), you can use `git checkout .` to remove all your changes or `git stash` to save your changes to a temporary location if you wish to keep them later on.

## Code guide

We divided the code in several branches:

* `git checkout master`: the very basics, hello world!
* `git checkout 01-modules`: installing and using modules
* `git checkout 02-express`: our first actual webserver
* `git checkout 03-with-templates`: using a template engine
* `git checkout 04-structure`: structuring our code
* `git checkout 05-static-users`: our first API (with hardcoded data)
* `git checkout 06-with-mongodb`: that same API (but with a persistant database)

## Testing your api with Postman

* download postman: http://tinyurl.com/postman3
* use the `builder` functionality
* enter a HTTP verb (`GET`, `PUT`, `POST`, `DELETE`)
* enter the url (most likely  http://localhost:3000/users)
* don't forget the `params` > `headers`:
** key: `Content-Type`
** value: `application/json`
* enter your json data if needed as RAW data, but make sure it is valid JSON, ...
