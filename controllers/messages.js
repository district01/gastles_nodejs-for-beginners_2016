var _ = require('lodash');
var Message = require('./../models/message');
var config = require('./../config/config.json');

// get all messages
function getAll(req, res, next) {
  var limit = req.query.limit || config.listLimit;
  limit = parseInt(limit, 10);

  Message
    .find({})
    .sort({ '_id' : -1 })
    .limit(limit)
    .exec(function onMessagesFound(err, messages) {
      // we return the json version with cleaned up model to the user instead of the entire model
      // see models/message.js for the json transform method.
      var transform = _.map(messages, function (message) {
        return message.toJSON();
      });
      transform = _.reverse(transform);
      res.send(transform);
    });
}
module.exports.read = getAll;

// get all messages from a room (req.params.room)
function getAllFromRoom(req, res, next) {
  var limit = req.query.limit || config.listLimit;
  limit = parseInt(limit, 10);

  Message
    .find({room: req.params.room})
    .sort({ '_id' : -1 })
    .limit(limit)
    .exec(function onMessagesFound(err, messages) {
      // we return the json version with cleaned up model to the user instead of the entire model
      // see models/message.js for the json transform method.
      var transform = _.map(messages, function (message) {
        return message.toJSON();
      });
      transform = _.reverse(transform);
      res.send(transform);
    });
}
module.exports.readFromRoom = getAllFromRoom;

// get one message by id (req.params.id)
function getOne(req, res, next) {
  Message.findOne({_id: req.params.id}, function onMessageFound(err, message) {
    if(!message) {
      res.status(404).send("Message not found");
    }

    // we return the json version with cleaned up model to the user
    res.send(message.toJSON());
  });
}
module.exports.readOne = getOne;

// create a new message (req.body)
function add(req, res, next) {
  var newMessage = new Message(req.body);

  newMessage.save(function onMessageSaved(err, message) {
    // we return the json version with cleaned up model to the user
    res.send(message.toJSON());
  });
}
module.exports.create = add;

// update a message (req.body)
function update(req, res, next) {
  Message.findOne({_id: req.params.id}, function onMessageFound(err, message) {
    if(!message) {
      return res.status(404).send("Message not found");
    }

    // now that we found the message, let's update the message
    // for every key that was send [Name, Content, ...]
    _.forEach(_.keys(req.body), function (key) {
      message.set(key, req.body[key]);
    });

    // and save it again.
    message.save(function (err, message) {
      // we return the json version with cleaned up model to the user
      res.send(message.toJSON());
    })
  });
}
module.exports.update = update;

// remove one message by id (req.params.id)
function remove(req, res, next) {
  Message.findOneAndRemove({_id: req.params.id}, function onMessageFound(err, message) {
    res.status(204).send();
  });
}
module.exports.remove = remove;

