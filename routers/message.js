var controller = require('./../controllers/messages');
var express = require('express');
var router = express.Router();

router.get('/', controller.read);
router.get('/:id', controller.readOne);
router.get('/room/:room', controller.readFromRoom);
router.post('/', controller.create);
router.put('/:id', controller.update);
router.delete('/:id', controller.remove);

module.exports = router;
